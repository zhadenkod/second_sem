package Task3_Input_Output;

import org.testng.annotations.Test;

import java.io.IOException;

import static Task3_Input_Output.HouseService.*;
import static org.testng.Assert.*;

public class HouseServiceTest {

    @Test
    public void testSerialiseHouse() {
        House house = new House("12345", "г.Омск, ул.Герцена, д.123",
                new Person("Григорьев", "Иван", "Иванович", "03.05.1979"),
                new Flat(10, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(27, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81"),
                        new Person("Заболотний", "Иван", "Романович", "10.05.83")),
                new Flat(14, 53, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(20, 48, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")));
        serialiseHouse(house, "serializedHouse.txt");
        assertEquals(house, deserialiseHouse("serializedHouse.txt"));
    }

    @Test
    public void testSaveHouse() {
        House house = new House("12345", "г.Омск, ул.Герцена, д.123",
                new Person("Григорьев", "Иван", "Иванович", "03.05.1979"),
                new Flat(10, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(27, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81"),
                        new Person("Заболотний", "Иван", "Романович", "10.05.83")),
                new Flat(14, 53, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(20, 48, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")));
        saveHouse(house);
    }

    @Test
    public void testJacksonSer() throws IOException {
        House house = new House("12345", "г.Омск, ул.Герцена, д.123",
                new Person("Григорьев", "Иван", "Иванович", "03.05.1979"),
                new Flat(10, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(27, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81"),
                        new Person("Заболотний", "Иван", "Романович", "10.05.83")),
                new Flat(14, 53, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(20, 48, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")));


        String ser = jacksonSer(house, "serHouse");
        House deser = jacksonDeser(ser);
        assertEquals(deser, house);
    }
}