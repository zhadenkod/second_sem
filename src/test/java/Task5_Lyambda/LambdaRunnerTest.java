package Task5_Lyambda;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import static Task5_Lyambda.LambdaDemo.*;
import static org.testng.Assert.*;

public class LambdaRunnerTest {

    @DataProvider
    public static Object [][] resFunction() {
        return new Object[][] {
                {strLength, "aaa", 3},
                {strFirstSymbol, "imit", 'i'},
                {wordCount, " fgfdgwa  ksf ufjn w", 4},
                {humanAge, new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE), 17},
                {humanFullName, new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE),
                        "Hugo A Fitz"},
                {olderHuman, new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE),
                        new Human("Hugo", "A", "Fitz", 18, Sex.FEMALE)}
        };
    }

    @DataProvider
    public static Object [][] resQuadroFunction() {
        return new Object[][] {
                {isHumansYoungerMaxAge,
                        new Human("Peet", "Anna", "Fitz", 10, Sex.FEMALE),
                        new Human("Peet", "Anna", "Fitz", 48, Sex.FEMALE),
                        new Human("Peet", "Anna", "Fitz", 17, Sex.FEMALE),
                        18, false},
                {isHumansYoungerMaxAge,
                        new Human("Peet", "Anna", "Fitz", 19, Sex.FEMALE),
                        new Human("Peet", "Anna", "Fitz", 21, Sex.FEMALE),
                        new Human("Peet", "Anna", "Fitz", 11, Sex.FEMALE),
                        22, true}
        };
    }

    @DataProvider
    public static Object [][] resPredicate() {
        return new Object[][] {
                {hasSpaces, "gfgeh", false},
                {hasSpaces, " f a r  ", true}
        };
    }

    @DataProvider
    public static Object [][] resBiPredicate() {
        return new Object[][] {
                {hasEgualSecondName,
                        new Human("Peet", "Lisa", "Violet", 19, Sex.FEMALE),
                        new Human("Lang", "Anna", "Fitz", 21, Sex.FEMALE),
                        false},
                {hasEgualSecondName,
                        new Human("Peet", "Lisa", "Violet", 19, Sex.FEMALE),
                        new Human("Peet", "Anna", "Fitz", 21, Sex.FEMALE),
                        true}
        };
    }

    @Test(dataProvider = "resFunction")
    public <T, U> void testResFunction(Function<T, U> function, T argument, U expected) {
        U actual = function.apply(argument);
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "resQuadroFunction")
    public <T, R, S, U, F> void testResQuadroFunction(QuadroFunction<T, R, S, U, F> function,
                                                     T arg1, R arg2, S arg3, U arg4, F expected) {
        F actual = function.apply(arg1, arg2, arg3, arg4);
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "resPredicate")
    public <T> void testResPredicate(Predicate<T> predicate, T argument, boolean expected) {
        boolean actual = predicate.test(argument);
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "resBiPredicate")
    public <T, U> void testResBiPredicate(BiPredicate<T, U> predicate, T arg1, U arg2, boolean expected) {
        boolean actual = predicate.test(arg1, arg2);
        assertEquals(actual, expected);
    }
}