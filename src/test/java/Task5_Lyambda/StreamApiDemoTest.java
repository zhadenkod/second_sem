package Task5_Lyambda;

import org.testng.annotations.Test;

import java.util.*;

import static Task5_Lyambda.Sex.*;
import static org.testng.Assert.*;

public class StreamApiDemoTest {
    @Test
    public void deleteNullObjectsTest() {
        List<Object> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add(null);
        list.add("");
        list.add(null);
        List<Object> exp = new ArrayList<>();
        exp.add("a");
        exp.add("b");
        exp.add("");
        assertEquals(exp, LambdaRunner.resFunction(StreamApiDemo.deleteNull, list));
        assertEquals(exp, LambdaRunner.resFunction(StreamApiDemo.deleteNull, exp));
        assertEquals(new ArrayList<>(), LambdaRunner.resFunction(StreamApiDemo.deleteNull, new ArrayList<>()));
    }

    @Test
    public void getPositiveNumbersAmount() {
        Set<Integer> integers = new HashSet<>();
        assertEquals(0, LambdaRunner.resFunction(StreamApiDemo.countPositive, integers).intValue());
        integers.add(-1);
        integers.add(0);
        assertEquals(0, LambdaRunner.resFunction(StreamApiDemo.countPositive, integers).intValue());
        integers.add(9);
        integers.add(23);
        assertEquals(2, LambdaRunner.resFunction(StreamApiDemo.countPositive, integers).intValue());
    }

    @Test
    public void getThreeLastElemsTest() {
        List<Object> list = new ArrayList<>();
        List<Object> last = new ArrayList<>();
        assertEquals(last, LambdaRunner.resFunction(StreamApiDemo.getThreeLastObjects, list));
        list.add("ddd");
        list.add("eee");
        last.add("ddd");
        last.add("eee");
        assertEquals(last, LambdaRunner.resFunction(StreamApiDemo.getThreeLastObjects, list));
        list.add("aaa");
        list.add("bbb");
        last.add("aaa");
        last.add("bbb");
        last.remove("ddd");
        assertEquals(last, LambdaRunner.resFunction(StreamApiDemo.getThreeLastObjects, list));
    }

    @Test
    public void getFirstEntryTest() {
        List<Integer> integers = new ArrayList<>();
        assertNull(LambdaRunner.resFunction(StreamApiDemo.getEven, integers));
        integers.add(1);
        integers.add(3);
        integers.add(5);
        assertNull(LambdaRunner.resFunction(StreamApiDemo.getEven, integers));
        integers.add(2);
        assertEquals(2, LambdaRunner.resFunction(StreamApiDemo.getEven, integers).intValue());
    }

    @Test
    public void getSquaresTest() {
        int[] nums = new int[]{1, 2, -2, 3};
        int[] empty = new int[0];
        List<Integer> squares = new ArrayList<>();
        squares.add(1);
        squares.add(4);
        squares.add(9);
        assertEquals(squares, LambdaRunner.resFunction(StreamApiDemo.getSquareList, nums));
        assertEquals(new ArrayList<>(), LambdaRunner.resFunction(StreamApiDemo.getSquareList, empty));
    }

    @Test
    public void getNoneEmptyListTest() {
        List<String> list = new ArrayList<>();
        list.add("");
        assertEquals(new ArrayList<>(), LambdaRunner.resFunction(StreamApiDemo.getNotEmptyStringList, list));
        list.add("aaa");
        list.add("sss");
        list.add("");
        List<String> result = new ArrayList<>();
        result.add("aaa");
        result.add("sss");
        assertEquals(result, LambdaRunner.resFunction(StreamApiDemo.getNotEmptyStringList, list));
    }

    @Test
    public void descendingSortedTest() {
        Set<String> strings = new HashSet<>();
        strings.add("aaa");
        strings.add("bbb");
        strings.add("ccc");
        List<String> reverseOrdered = new ArrayList<>();
        reverseOrdered.add("ccc");
        reverseOrdered.add("bbb");
        reverseOrdered.add("aaa");
        assertEquals(reverseOrdered, LambdaRunner.resFunction(StreamApiDemo.getOrderedList, strings));
    }

    @Test
    public void getSquaresSum() {
        Set<Integer> integers = new HashSet<>();
        integers.add(1);
        integers.add(2);
        integers.add(-2);
        assertEquals(9, LambdaRunner.resFunction(StreamApiDemo.getSquaresSum, integers).intValue());
    }

    @Test
    public void getMaxAgeUseStreamAPITest() {
        List<Human> humans = new ArrayList<>();
        assertEquals(-1, LambdaRunner.resFunction(StreamApiDemo.getMaxAge, humans).intValue());
        humans.add(new Human("aaa", "bbb", "ccc", 18, FEMALE));
        assertEquals(18, LambdaRunner.resFunction(StreamApiDemo.getMaxAge, humans).intValue());
        humans.add(new Human("aaa", "bbb", "ccc", 20, FEMALE));
        assertEquals(20, LambdaRunner.resFunction(StreamApiDemo.getMaxAge, humans).intValue());
    }

    @Test
    public void sortBySexAndAge() {
        List<Human> humans = new ArrayList<>();
        List<Human> result = new ArrayList<>();
        assertEquals(result, LambdaRunner.resFunction(StreamApiDemo.getSorted, humans));
        humans.add(new Human("aaa", "bbb", "ccc", 18, FEMALE));
        humans.add(new Human("eee", "bbb", "ccc", 18, MALE));
        humans.add(new Human("mmm", "bbb", "ccc", 20, FEMALE));
        humans.add(new Human("aaa", "lll", "ccc", 16, MALE));
        result.add(new Human("aaa", "lll", "ccc", 16, MALE));
        result.add(new Human("eee", "bbb", "ccc", 18, MALE));
        result.add(new Human("aaa", "bbb", "ccc", 18, FEMALE));
        result.add(new Human("mmm", "bbb", "ccc", 20, FEMALE));
        assertEquals(result, LambdaRunner.resFunction(StreamApiDemo.getSorted, humans));
    }

}