package Task5_Lyambda;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static Task5_Lyambda.LambdaDemo.*;
import static org.testng.Assert.*;

public class LambdaDemoTest {
    @DataProvider
    public static Object [][] stringsLen() {
        return new Object[][] {
                {"", 0},
                {" ", 1},
                {"aaa", 3},
                {"a b c", 5}
        };
    }

    @DataProvider
    public static Object [][] stringsFirstSymbol() {
        return new Object[][] {
                {" ", ' '},
                {"aaa", 'a'},
                {"5 b c", '5'},
                {"&uauuui", '&'}
        };
    }

    @DataProvider
    public static Object [][] stringsHasSpaces() {
        return new Object[][] {
                {"", false},
                {" ", true},
                {"aaa", false},
                {"5 b c", true},
                {"&uauuui", false}
        };
    }

    @DataProvider
    public static Object [][] stringsWordCount() {
        return new Object[][] {
                {"", 0},
                {" ", 1},
                {"aaa", 1},
                {"5, b, c", 3},
                {"&uauuui", 1}
        };
    }

    @DataProvider
    public static Object [][] humanAge() {
        return new Object[][] {
                {new Human("Smith", "Adam", "Fitz", 45, Sex.MALE), 45},
                {new Human("Peet", "Anna", "Fitz", 4, Sex.FEMALE), 4},
                {new Student("Wirt", "Sam", "Fitz", 20, Sex.MALE,
                        "OmSU", "Computer Science", "Informatics"), 20},
                {new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE), 17}
        };
    }

    @DataProvider
    public static Object [][] humanHasEqualsSecondName() {
        return new Object[][] {
                {new Human("Smith", "Andy", "Greg", 37, Sex.MALE),
                        new Human("Smith", "Adam", "Fitz", 45, Sex.MALE), true},
                {new Human("Peet", "Anna", "Fitz", 14, Sex.FEMALE),
                        new Human("Pat", "Lisa", "Fitz", 4, Sex.FEMALE), false},
                {new Student("Wirt", "Sam", "Fitz", 20, Sex.MALE, "OmSU", "Computer Science", "Informatics"),
                        new Student("Jonson", "Sam", "Fitz", 20, Sex.MALE, "OmSU", "Computer Science", "Informatics"), false},
                {new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE),
                new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE), true}
        };
    }

    @DataProvider
    public static Object [][] humanFullName() {
        return new Object[][] {
                {new Human("Smith", "Adam", "Fitz", 45, Sex.MALE),
                        "Smith Adam Fitz"},
                {new Human("Peet", "Anna", "Fitz", 4, Sex.FEMALE),
                        "Peet Anna Fitz"},
                {new Student("Wirt", "Sam", "Fitz", 20, Sex.MALE,
                        "OmSU", "Computer Science", "Informatics"),
                        "Wirt Sam Fitz"},
                {new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE),
                        "Hugo A Fitz"}
        };
    }

    @DataProvider
    public static Object [][] humanOlderHuman() {
        return new Object[][] {
                {new Human("Smith", "Adam", "Fitz", 45, Sex.MALE),
                        new Human("Smith", "Adam", "Fitz", 46, Sex.MALE)},
                {new Human("Peet", "Anna", "Fitz", 4, Sex.FEMALE),
                        new Human("Peet", "Anna", "Fitz", 5, Sex.FEMALE)},
                {new Student("Wirt", "Sam", "Fitz", 20, Sex.MALE,
                        "OmSU", "Computer Science", "Informatics"),
                        new Student("Wirt", "Sam", "Fitz", 21, Sex.MALE,
                                "OmSU", "Computer Science", "Informatics")},
                {new Human("Hugo", "A", "Fitz", 17, Sex.FEMALE),
                        new Human("Hugo", "A", "Fitz", 18, Sex.FEMALE)}
        };
    }

    @DataProvider
    public static Object [][] humanIsHumansYoungerMaxAge() {
        return new Object[][] {
                {new Human("Smith", "Adam", "Fitz", 45, Sex.MALE),
                 new Human("Smith", "Adam", "Fitz", 30, Sex.MALE),
                 new Human("Smith", "Adam", "Fitz", 38, Sex.MALE),
                        47, true},
                {new Human("Peet", "Anna", "Fitz", 4, Sex.FEMALE),
                 new Human("Peet", "Anna", "Fitz", 40, Sex.FEMALE),
                 new Human("Peet", "Anna", "Fitz", 17, Sex.FEMALE),
                        18, false},
                {new Student("Wirt", "Sam", "Fitz", 28, Sex.MALE,
                        "OmSU", "Computer Science", "Informatics"),
                        new Student("Wirt", "Sam", "Fitz", 10, Sex.MALE,
                                "OmSU", "Computer Science", "Informatics"),
                        new Student("Wirt", "Sam", "Fitz", 32, Sex.MALE,
                                "OmSU", "Computer Science", "Informatics"),
                        34, true}
        };
    }

    @Test (dataProvider = "stringsLen")
    public void testStrLentgh(String str, int expected){
        int actual = strLength.apply(str);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "stringsFirstSymbol")
    public void testStrFirstSymbol(String str, char expected){
        char actual = strFirstSymbol.apply(str);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "stringsHasSpaces")
    public void testHasSpaces(String str, boolean expected){
        boolean actual = hasSpaces.test(str);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "stringsWordCount")
    public void testSpaceCount(String str, int expected){
        int actual = wordCount.apply(str);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "humanAge")
    public void testHumanAge(Human human, int expected){
        int actual = humanAge.apply(human);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "humanHasEqualsSecondName")
    public void testHasEqualsSecondName(Human human1, Human human2, boolean expected){
        boolean actual = hasEgualSecondName.test(human1, human2);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "humanFullName")
    public void testHumanFullName(Human human, String expected){
        String actual = humanFullName.apply(human);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "humanOlderHuman")
    public void testOlderHuman(Human human, Human expected){
        Human actual = olderHuman.apply(human);
        assertEquals(actual, expected);
    }

    @Test (dataProvider = "humanIsHumansYoungerMaxAge")
    public void testIsHumansYoungerMaxAge(Human human1, Human human2, Human human3, int MaxAge, boolean expected){
        boolean actual = isHumansYoungerMaxAge.apply(human1, human2, human3, MaxAge);
        assertEquals(actual, expected);
    }

}