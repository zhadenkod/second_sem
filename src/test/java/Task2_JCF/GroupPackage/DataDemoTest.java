package Task2_JCF.GroupPackage;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static Task2_JCF.GroupPackage.DataDemo.getAll;
import static org.testng.Assert.*;

public class DataDemoTest {

    @Test
    public void testGetAll() {
        List<Integer> expected = new ArrayList<>();
        Collections.addAll(expected, 1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15, 16, 5, 0, 0, 0, 7, 20, 19, 18, 17, 16, 1);
        Data data = new Data("GroupSet",
                new Group(100, 1, 2, 3, 4, 5),
                new Group(101, 10, 11, 12, 13, 14, 15, 16),
                new Group(102, 5, 0, 0, 0, 7),
                new Group(103, 20, 19, 18, 17, 16, 1));
        assertEquals(getAll(data), expected);
    }
}