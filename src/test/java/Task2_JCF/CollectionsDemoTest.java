package Task2_JCF;


import org.hamcrest.Matcher;
import org.testng.annotations.Test;

import java.util.*;

import static Task2_JCF.CollectionsDemo.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.testng.Assert.*;

public class CollectionsDemoTest {

    @Test
    public void testCountStringsWithFirstSymbol1() throws NullPointerException {
        List<String> strings = new ArrayList<>();
        Collections.addAll(strings,
                "1234", "f retui", "12 3 4 5", " 1  2   3",
                "ffff", "fqyuw eerere");
        assertEquals(2, countStringsWithFirstSymbol(strings, '1'));
        assertEquals(3, countStringsWithFirstSymbol(strings, 'f'));
        assertEquals(0, countStringsWithFirstSymbol(strings, 'F'));
        assertEquals(1, countStringsWithFirstSymbol(strings, ' '));
    }

    @Test (expectedExceptions = {NullPointerException.class})
    public void testCountStringsWithFirstSymbol2() throws NullPointerException {
        List<String> strings = new ArrayList<>();
        Collections.addAll(strings,
                "121", "", null);
        countStringsWithFirstSymbol(strings, '1');
    }

    @Test
    public void testHomonyms() {
    }

    @Test
    public void testDeleteHuman() {
    }

    @Test
    public void testDisjointSets() {
    }

    @Test
    public void testMaxAgeSet() {
        List<Human> list = new ArrayList<>();
        Collections.addAll(list,
            new Human("Smith", "Adam", "Fitz", 45),
            new Human("Peet", "Adam", "Fitz", 4),
            new Student("Wirt", "Sam", "Fitz", 45, "qwert"),
            new Human("Hugo", "A", "Fitz", 17),
            new Human("Aaa", "Bbb", "Ccc", 18)
        );
        List<Human> expected = new ArrayList<>();
        Collections.addAll(expected,
            new Student("Wirt", "Sam", "Fitz", 45, "qwert"),
            new Human("Smith", "Adam", "Fitz", 45)
        );
        Set<Human> actual = maxAgeSet(list);
        assertThat(actual, equalTo(expected));
    }

    private void assertThat(Set<Human> actual, Matcher<List<Human>> equalTo) {
    }

    @Test
    public void testChooseHumansByNumbers() {
    }

    @Test
    public void testAgeMore18() {
        Map<Integer, Human> humansMap = new HashMap<>();
        humansMap.put(1, new Human("Smith", "Adam", "Fitz", 45));
        humansMap.put(2, new Human("Peet", "Adam", "Fitz", 4));
        humansMap.put(3, new Human("Wirt", "Sam", "Fitz", 1));
        humansMap.put(4, new Human("Hugo", "A", "Fitz", 17));
        humansMap.put(5, new Human("Aaa", "Bbb", "Ccc", 18));
        List<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(5);
        assertEquals(ageMore18(humansMap), expected);
    }

    @Test
    public void testGetIdAgeMap() {
        Map<Integer, Human> humansMap = new HashMap<>();
        humansMap.put(1, new Human("Smith", "Adam", "Fitz", 45));
        humansMap.put(2, new Human("Peet", "Adam", "Fitz", 17));
        humansMap.put(3, new Human("Wirt", "Sam", "Fitz", 1));
        humansMap.put(4, new Human("Hugo", "A", "Fitz", 17));
        humansMap.put(5, new Human("Aaa", "Bbb", "Ccc", 18));
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(1, 45);
        expected.put(2,17);
        expected.put(3,1);
        expected.put(4,17);
        expected.put(5,18);
        assertEquals(getIdAgeMap(humansMap), expected);
    }

    @Test
    public void testGetAgeHumanMap() {
    }


    @Test
    public void testGetAgeCharacterHumanMap() {
        Set<Human> humanSet = new HashSet<>();
        //humanSet.add(new Human("Jones", "Adam", "Fitz", 35));
        humanSet.add(new Human("Smith", "Adam", "Fitz", 20));
        humanSet.add(new Human("Simon", "David", "Fitz", 20));
        humanSet.add(new Human("Brown", "Adam", "Fitz", 20));
        humanSet.add(new Human("Smith", "Adam", "Fitz", 18));
        humanSet.add(new Human("Stephen", "Adam", "Fitz", 20));
        humanSet.add(new Human("Smith", "Andy", "Fitz", 18));
        Map<Integer, Map<Character, List<Human>>> expected = new HashMap<>();
        Map<Character, List<Human>> twentyMap = new HashMap<>();
        List<Human> s20List = new ArrayList<>();
        s20List.add(new Human("Simon", "David", "Fitz", 20));
        s20List.add(new Human("Smith", "Adam", "Fitz", 20));
        s20List.add(new Human("Stephen", "Adam", "Fitz", 20));
        twentyMap.put('S', s20List);
        List<Human> b20List = new ArrayList<>();
        b20List.add(new Human("Brown", "Adam", "Fitz", 20));
        twentyMap.put('B', b20List);
        Map<Character, List<Human>> eighteenMap = new HashMap<>();
        List<Human> s18List = new ArrayList<>();
        s18List.add(new Human("Smith", "Adam", "Fitz", 18));
        s18List.add(new Human("Smith", "Andy", "Fitz", 18));
        eighteenMap.put('S', s18List);
        expected.put(20, twentyMap);
        expected.put(18, eighteenMap);
        System.out.println(getAgeCharacterHumanMap(humanSet).toString());
        System.out.println(expected.toString());
        assertEquals(getAgeCharacterHumanMap(humanSet), expected);
    }
}