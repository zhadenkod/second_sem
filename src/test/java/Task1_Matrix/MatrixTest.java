package Task1_Matrix;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class MatrixTest {
    private final double delta = 0.01;

    @DataProvider
    public static Object[][] matrix () {
        return new Object[][] {
            {new Matrix(1.0, 2.0, 3.0, 7.0, 10.0, 1.0, 4.0, 8.0, 5.0), 28},
            {new Matrix(5.0, 13.0, -3.0, 4.1, 10.7, 6.0, 4.0, 0.0, 2.0), 440.8},
            {new Matrix(5), 1},
            {new Matrix (2.0, 8.0, 5.0, 0.0, 0.0, 0.0, 7.0, 7.0, 3.0), 0},
            {new Matrix (2.0, 0.0, 5.0, 1.0, 0.0, 5.0, 7.0, 0.0, 3.0), 0},
            {new Matrix (1.0, 2.0, 3.0, 0.0, 0.0, 4.0, 0.0, 0.0, 1.0), 0}
        };
    }

    @DataProvider
    public static Object[][] invertMatrix () {
        return new Object[][] {
            {new InvertableMatrix(new Matrix(1.0, 2.0, 3.0, 7.0, 10.0, 1.0, 4.0, 8.0, 5.0)),
                    new InvertableMatrix(new Matrix(1.5, 0.5, -1, -31/28.0, -0.25, 5/7.0, 4/7.0, 0, -1/7.0))},
                {new InvertableMatrix(new Matrix(1.0, 2.0, 2.0, 1.0, 3.0, 3.0, 1.0, 5.0, 7.0)),
                        new InvertableMatrix(new Matrix(3.0, -2.0, 0.0, -2.0, 2.5, -0.5, 1.0, -1.5, 0.5))},
                {new InvertableMatrix(new Matrix(2.0, 5.0, 7.0, 6.0, 3.0, 4.0, 5.0, -2.0, -3.0)),
                        new InvertableMatrix(new Matrix(1.0, -1.0, 1.0, -38.0, 41.0, -34.0, 27.0, -29.0, 24.0))}
        };
    }

    @Test(dataProvider = "matrix")
    public void testDetermMatrix(Matrix matrix, double expected) {
        double actual = matrix.calcDeterminant();
        assertEquals(actual, expected, delta);
    }

    @Test(dataProvider = "invertMatrix")
    public void testInvertMatrix(InvertableMatrix matrix, InvertableMatrix expected) throws IncorrectIndexException, InvertableMatrixException {
        InvertableMatrix actual = matrix.getInvertMatrix();
        assertEquals(actual, expected);
    }
}
