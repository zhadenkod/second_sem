package Task1_Matrix;

import java.io.*;
import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Scanner;

public class DemoMatrix {

    public static void showMatrix(Matrix matrix, Writer w) throws IncorrectIndexException, IOException {
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = 0; j < matrix.getSize(); j++) {
                w.write(matrix.getElem(i, j) + " ");
            }
            w.write("\n");
        }
        w.close();
    }
    public static Matrix readMatrix(Reader reader) throws IOException {
        BufferedReader br = new BufferedReader(reader);
        ArrayList<Double> list = new ArrayList<>();
        while(br.ready()){
            String line = br.readLine();
            String[] lineArray = line.split(" ");
            for (String elem: lineArray) {
                list.add(Double.parseDouble(elem));
            }
        }
        double[] arr = new double[list.size()];
        int i=0;
        for (Double d : list) {
            arr[i++] = d;
        }
        return new Matrix(arr);
    }
    public static double sumOfAllMatrixElems(Matrix matrix) throws IncorrectIndexException {
        double sum = 0.0;
        for(int i = 0; i < matrix.getSize(); i++) {
            for(int j = 0; j < matrix.getSize(); j++) {
                sum += matrix.getElem(i, j);
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        Matrix matrix1 = new Matrix(1, 2, 3, 0, 5, 7, 0, 0, 9);
        try {
            StringWriter stringWriter = new StringWriter();
            showMatrix(matrix1, stringWriter);
            Matrix resMatrix = readMatrix(new CharArrayReader(stringWriter.toString().toCharArray()));
            System.out.println(matrix1.equals(resMatrix));
        } catch (IOException e) {
            e.getMessage();
        }

        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("output.txt"))) {
            out.writeObject(matrix1);
            out.flush();
        } catch (IOException e) {
            System.out.println("Serialization error!");
        }
        try (ObjectInputStream serIn = new ObjectInputStream(new FileInputStream("output.txt"))) {
            Matrix matrixSer = (Matrix) serIn.readObject();
            System.out.println(matrix1.equals(matrixSer));
        } catch (IOException e) {
            System.out.println("Deserialization error!");
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found Error!");
        }
    }
}
