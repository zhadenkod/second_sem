package Task1_Matrix;

public interface IInvertableMatrix extends IMatrix {
    IInvertableMatrix getInvertMatrix() throws InvertableMatrixException, IncorrectIndexException;
}
