package Task1_Matrix;

public class InvertableMatrixException extends MatrixException {

    public InvertableMatrixException() {super("The matrix is not invertable");}
    public InvertableMatrixException(Throwable cause) {
        super(cause);
    }
}
