package Task1_Matrix;

import static java.lang.Math.pow;

public class InvertableMatrix extends Matrix implements IInvertableMatrix {

    public InvertableMatrix(int n) {
        super(n);
    }

    public InvertableMatrix(Matrix matrix) {
        super(matrix);
        if (this.calcDeterminant() == 0.0) {
            throw new InvertableMatrixException();
        }
    }

    public InvertableMatrix getInvertMatrix() throws InvertableMatrixException, IncorrectIndexException {
        if(calcDeterminant() == 0) {
            throw new InvertableMatrixException();
        } else {
            double coeff = 1.0/getDeterminant();
            InvertableMatrix invertMatrix = new InvertableMatrix(getSize());
            for(int i = 0; i < getSize(); i++) {
                for(int j = 0; j < getSize(); j++) {
                    Matrix minor = new Matrix(getSize() - 1);
                    int string = 0;
                    boolean isAddS = false;
                    for(int mi = 0; mi < getSize() - 1; mi++) {
                        int column = 0;
                        boolean isAddC = false;
                        for(int mj = 0; mj < getSize() - 1; mj++) {
                            if(!isAddS) {
                                if(mi == i) {
                                    string++;
                                    isAddS = true;
                                }
                            }
                            if(!isAddC) {
                                if(mj == j) {
                                    column++;
                                    isAddC = true;
                                }
                            }
                            minor.setElem(mi, mj, getElem(string, column));
                            column++;
                        }
                        string++;
                    }
                    double elem = minor.calcDeterminant()*pow(-1, i + j);
                    invertMatrix.setElem(j, i, elem*coeff);
                }
            }
            return invertMatrix;
        }
    }
}
