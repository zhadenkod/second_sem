package Task1_Matrix;

public interface IMatrix {
    double getElem(int i, int j) throws IncorrectIndexException;
    void setElem(int i, int j, double a) throws IncorrectIndexException;
    double calcDeterminant();

}
