package Task1_Matrix;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

import static java.lang.Math.abs;

public class Matrix implements IMatrix, Serializable {

    private double [] elems;
    private int size;
    private double determinant;
    private boolean isActualDeterm;

    private Matrix() {}

    /**
     *
     * @param n size
     */
    public Matrix(int n) {
        if(n >= 0) {
            size = n;
            elems = new double[n * n];
            for (int i = 0; i < size; i++) {
                this.elems[i * size + i] = 1.0;
            }
            isActualDeterm = false;
        } else {
            throw new MatrixException("Size is negative");
        }
    }
    public Matrix(Matrix matrix) {
        size = matrix.getSize();
        elems = new double[matrix.getSize()*matrix.getSize()];
        for(int i = 0; i < matrix.getSize(); i++) {
            for(int j = 0; j < matrix.getSize(); j++) {
                elems[i*getSize() + j] = matrix.getElem(i, j);
            }
        }
        if(matrix.isActualDeterm) {
            determinant = matrix.getDeterminant();
        } else {
            isActualDeterm = false;
        }
    }
    public Matrix(double... arg) {
        size = (int) Math.floor(Math.sqrt(arg.length));
        isActualDeterm = false;
        this.elems = new double[size * size];
        System.arraycopy(arg, 0, this.elems, 0, size * size);
    }

    private void firstLineAddSecond(double[] buff, int firstLine, int secondLine, double coeff) {
        for(int j = 0; j < size; j++) {
            buff[firstLine*size + j] += coeff * buff[secondLine*size + j];
        }
    }
    private double countCoeff(double [] buff, int i1, int i2) {
        if (buff[i1*size + i1] == 0) {
            return 1;
        } else {
            return -(buff[i2*size + i1]/buff[i1*size + i1]);
        }
    }
    private int notZeroElem(double [] buff, int column) {
        for(int i = size-1; i > column; i--) {
            if (buff[i*size + column] != 0) {
                return i;
            }
        }
        return column;
    }

    public double getElem(int i, int j) throws IncorrectIndexException {
        if (i < 0 || j < 0 || i >= size || j >= size) {
            throw new IncorrectIndexException();
        }
        return elems[i*size + j];

    }

    public void setElem(int i, int j, double a) {
        if (i < 0 || j < 0 || i >= size || j >= size) {
            throw new IncorrectIndexException();
        }
        elems[i*size + j] = a;
        isActualDeterm = false;
    }

    public double getDeterminant() {
        return isActualDeterm ? determinant : calcDeterminant();
    }

    public int getSize() {
        return size;
    }

    public double calcDeterminant() {
        if(isActualDeterm) {
            return determinant;
        } else {
            double[] buff = new double[size * size];

            System.arraycopy(elems, 0, buff, 0, size * size);
            for (int i = 0; i < size; i++) {
                if (buff[i * size + i] == 0) {
                    firstLineAddSecond(buff, i, notZeroElem(buff, i), countCoeff(buff, i, notZeroElem(buff, i)));
                }
                for (int k = i + 1; k < size; k++) {
                    firstLineAddSecond(buff, k, i, countCoeff(buff, i, k));
                }
            }
            double determ = 1.0;
            for (int i = 0; i < size; i++) {
                determ *= buff[i * size + i];
            }
            determinant = determ;
            isActualDeterm = true;
            return determ;
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                str.append(elems[i*size + j]).append(" ");
            }
            str.append("\n");
        }
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix matrix = (Matrix) o;
        boolean isEqual = true;
        for(int i = 0; (i < size * size) && isEqual; i++) {
            isEqual = Math.abs(this.elems[i] - matrix.elems[i]) < 1e-10;
        }
        return size == matrix.size && isEqual;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(size);
        result = 31 * result + Arrays.hashCode(elems);
        return result;
    }


}
