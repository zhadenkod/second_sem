package Task1_Matrix;

public class IncorrectIndexException extends MatrixException {
    public IncorrectIndexException() {
        super("Incorrect index");
    }
    public IncorrectIndexException(Throwable cause) {
        super(cause);
    }
}
