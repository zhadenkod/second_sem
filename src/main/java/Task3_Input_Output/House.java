package Task3_Input_Output;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@JsonAutoDetect
public class House implements Serializable {
    private String houseNumber;
    private String adress;
    private Person seniorHousework;
    private List<Flat> flatList;

    public House() {
//        this.seniorHousework = new Person("qqq", "fdfdfd", "dddd", "22.05.2019");
//        this.flatList = ArrayList<>();
    }
    public House(String houseNumber, String adress, Person seniorHousework, Flat ... flats) {
        this.houseNumber = houseNumber;
        this.adress = adress;
        this.seniorHousework = seniorHousework;
        flatList = new ArrayList<>();
        Collections.addAll(flatList, flats);
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getAdress() {
        return adress;
    }

    public Person getSeniorHousework() {
        return seniorHousework;
    }

    public List<Flat> getFlatList() {
        return flatList;
    }

    @Override
    public String toString() {
        return "House №" +
                houseNumber +
                ", " + adress  +
                "\nSenior housework: " + seniorHousework +
                "\n Flats: " + flatList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(houseNumber, house.houseNumber) &&
                Objects.equals(adress, house.adress) &&
                Objects.equals(seniorHousework, house.seniorHousework) &&
                Objects.equals(flatList, house.flatList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(houseNumber, adress, seniorHousework, flatList);
    }
}
