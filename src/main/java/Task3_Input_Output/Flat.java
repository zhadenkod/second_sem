package Task3_Input_Output;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Flat implements Serializable {
    private int flatNumber;
    private float flatArea;
    private List<Person> tenantList;

    public Flat() {
    }

    public Flat(int flatNumber, float flatArea, Person ... tenants) { //is it right?
        this.flatNumber = flatNumber;
        this.flatArea = flatArea;
        tenantList = new ArrayList<>();
        Collections.addAll(tenantList, tenants);
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public float getFlatArea() {
        return flatArea;
    }

    public List<Person> getTenantList() {
        return tenantList;
    }

    @Override
    public String toString() {
        return "Flat № " + flatNumber +
                ", " + flatArea + "square m" +
                "\nTenants" + tenantList ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;
        Flat flat = (Flat) o;
        return getFlatNumber() == flat.getFlatNumber() &&
                Float.compare(flat.getFlatArea(), getFlatArea()) == 0 &&
                Objects.equals(getTenantList(), flat.getTenantList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlatNumber(), getFlatArea(), getTenantList());
    }
}
