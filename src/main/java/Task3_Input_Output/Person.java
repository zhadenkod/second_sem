package Task3_Input_Output;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable {
    private String secondName;
    private String firstName;
    private String middleName;
    private String birthDate;

    public Person() {
    }
    public Person(String secondName, String firstName, String middleName, String birthDate) {
        this.secondName = secondName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDate = birthDate;
    }

    public Person(Person copy) {
        secondName = copy.secondName;
        firstName = copy.firstName;
        middleName = copy.middleName;
        birthDate = copy.birthDate;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFirstName() {
        return firstName;
    }


    public String getMiddleName() {
        return middleName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(getSecondName(), person.getSecondName()) &&
                Objects.equals(getFirstName(), person.getFirstName()) &&
                Objects.equals(getMiddleName(), person.getMiddleName()) &&
                Objects.equals(getBirthDate(), person.getBirthDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSecondName(), getFirstName(), getMiddleName(), getBirthDate());
    }

    @Override
    public String toString() {
        return secondName +
                " " + firstName +
                " " + middleName;
    }
}
