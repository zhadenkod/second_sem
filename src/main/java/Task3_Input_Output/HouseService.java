package Task3_Input_Output;

import au.com.bytecode.opencsv.CSVWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.sun.xml.internal.ws.encoding.soap.DeserializationException;
//import com.sun.xml.internal.ws.encoding.soap.SerializationException;

import java.io.*;
import java.util.Collections;
import java.util.Iterator;

public class HouseService {
    /**
     *
     * @param house house for serialization
     * @param fileName filename for serialized house
     */
    public static void serialiseHouse(House house, String fileName) {
        try (ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
            out.writeObject(house);
            out.flush();
        } catch (IOException e) {
            System.out.println("Serialization error!");
            //throw new SerializationException(e);
        }
    }

    public static House deserialiseHouse(String fileName) {
        try(ObjectInput in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fileName)))) {
            House res = (House) in.readObject();
            return res;
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Deserialization error!");
            //throw new DeserializationException(e);
        }
        //NEED TO BE COMMENTED
        return new House("0", "0", new Person("0", "0", "0", "0"));
    }

    /**
     *
     * @param house house for writing to csv-file
     */
    //FIXME to end writing saving to csv-file
    public static void saveHouse(House house) {
        String fileName = "house_" + house.getHouseNumber() + ".csv";
        Iterator<Flat> iterator = house.getFlatList().iterator();
        try (CSVWriter writer = new CSVWriter(new FileWriter(new File(fileName)))) {
            //header
            String[] houseData = new String[] {
                    "Данные о доме\n",
                    "Кадастровый номер: " + house.getHouseNumber() + "\n",
                    "Адрес:   " + house.getAdress() +"\n",
                    "Старший по дому:   " + house.getSeniorHousework().toString() + "\n",
                    "Данные о квартирах\n",
                    "№; Площадь, кв.м.; Владельцы\n"
            };
            writer.writeAll(Collections.singletonList(houseData));

            //table
            while(iterator.hasNext()) {
                Flat flat = iterator.next();
                String[] flatString = new String[] {
                    flat.getFlatNumber() + ";" +
                    flat.getFlatArea() + ";" +
                    flat.getTenantList().toString()
                };
                writer.writeNext(flatString);
            }
        } catch (IOException e) {
            System.out.println("File error!");
        }
    }

    public static String jacksonSer(House house, String str) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            str = mapper.writeValueAsString(house);
//            Writer writer = new BufferedWriter(new FileWriter(filename));
//            mapper.writeValue(writer, house);
        } catch (IOException e){
            e.getMessage();
        }
        return str;
    }

    public static House jacksonDeser(String str) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
//        try {
            return mapper.readValue(str, House.class);
//        } catch (IOException e) {
//            e.getMessage();
//
//        }
    }
}