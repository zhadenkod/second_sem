package Task3_Input_Output;

import java.io.*;

public class InputOutputDemo {

    public static void writeOutputStream(OutputStream out, int[] array) throws IOException{
        DataOutputStream output = new DataOutputStream(out);
        for (int elem : array) {
            output.writeInt(elem);
        }
    }

    public static int[] readByteArrayInputStream(InputStream input, int[] array) throws IOException{
        //int[] array = new int[n];
        DataInputStream dis = new DataInputStream(input);
        for(int i = 0; i < array.length ; i++) {
            array[i] = dis.readInt();
        }
        return array;
    }

    public static void writeCharArrayWriter(Writer writer, int[] array) throws IOException{
        PrintWriter output = new PrintWriter(writer);
        for(int i = 0; i < array.length; i++) {
            output.printf(String.format("%d ", array[i]));
//            output.write((char)array[i]);
//            output.write(' ');
        }
    }

    public static void readCharArrayReader(Reader input, int[] array) throws IOException{
        BufferedReader br = new BufferedReader(input);
        if (br.ready()) {
            String[] strArray = br.readLine().split(" ");
            for(int i = 0; i < array.length; i++) {
                array[i] = Integer.parseInt(strArray[i]);
            }
        }
    }

    public static int[] readArrayFromPosition(long position, RandomAccessFile  file) throws FileNotFoundException, IOException {
        file.seek(position);
        int len = (int) ((file.length() - position) / Integer.BYTES);
        int[] array = new int[len];

        for(int i = 0; (i < file.length() - position) ; i++) {
            array[i] = file.readInt();
        }
        return array;
    }

    public static File[] findFiles(String ext, File directory) {
        return directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(ext);
            }
        });
    }
}
