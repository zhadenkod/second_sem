package Task2_JCF;

import java.util.Comparator;

public class HumanNamesComparator implements Comparator<Human> {
    @Override
    public int compare(Human human1, Human human2) {
        if(human1.getSecondName().compareTo(human2.getSecondName()) == 0) {
            if(human1.getFirstName().compareTo(human2.getFirstName()) == 0) {
                return human1.getMiddleName().compareTo(human2.getMiddleName());
            }
            return human1.getFirstName().compareTo(human2.getFirstName());
        }
        return human1.getSecondName().compareTo(human2.getSecondName());
    }
}
