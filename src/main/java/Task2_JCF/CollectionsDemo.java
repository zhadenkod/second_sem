package Task2_JCF;

import java.util.*;

/**
 *
 */
public class CollectionsDemo {
    /**
     * 1.
     * @param strings list of String
     * @param symbol char
     * @return count
     */
    public static int countStringsWithFirstSymbol(List<String> strings, char symbol) throws NullPointerException {
        for(String string: strings) {
            if (string.equals(null)||string.equals("")) {
                throw new NullPointerException("Empty string!");
            }
        }
        int count = 0;
        for(int i = 0; i < strings.size(); i++) {
            if(strings.get(i).charAt(0) == symbol) {
                count++;
            }
        }
        return count;
    }

    /**
     * 2.
     * @param list
     * @param human
     * @return
     */
    public static List<Human> homonyms(List<Human> list, Human human) {
        List<Human> homonymList = new ArrayList<Human>();
        for (Human human1 : list) {
            if (human1.getSecondName().equals(human.getSecondName())) {
                homonymList.add(human1);
            }
        }
        return homonymList;
    }

    /**
     * 3.
     * @param list
     * @param human
     * @return
     */
    public static List<Human> deleteHuman(List<Human> list, Human human) {
        List<Human> newList = new ArrayList<Human>();
        for (Human human1 : list) {
            if (!human.equals(human1)) {
                newList.add(human1);
            }
        }
        return newList;
    }

    /**
     * 4.
     * @param list
     * @param set
     * @return
     */
    public static List<Set<Integer>> disjointSets(List<Set<Integer>> list, Set<Integer> set) {
        List<Set<Integer>> newList = new ArrayList<Set<Integer>>();
        int index = 0;
        Iterator<Integer> iterator;
        for (Set<Integer> integers : list) {
            boolean areNotIntersected = true; // RENAME
            iterator = integers.iterator();
            while (iterator.hasNext() && areNotIntersected) {
                if (set.contains(iterator.next())) {
                    areNotIntersected = false;
                }
            }
            if (areNotIntersected) {
                newList.add(index, integers);
                index++;
            }
//            areNotIntersected = true;
        }
        return newList;
    }

    /**
     * 5.
     * @param list
     * @return
     */
    public static Set<Human> maxAgeSet(List<Human> list) {
        Set<Human> set = new HashSet<Human>();
        int maxAge = 0;
        for (Human human : list) {
            if (human.getAge() > maxAge) {
                maxAge = human.getAge();
            }
        }
        for (Human human : list) {
            if (human.getAge() == maxAge) {
                set.add(human);
            }
        }
        return set;
    }

    /**
     * 7.
     * @param humanMap
     * @param set
     * @return
     */
    public static Set<Human> chooseHumansByNumbers(Map<Integer, Human> humanMap, Set<Integer> set) {
        Set<Human> result = new HashSet<>();
        Iterator<Integer> iterator = set.iterator();
        int id;
        while (iterator.hasNext()) {
            id = iterator.next();
            if(humanMap.get(id) != null) {
                result.add(humanMap.get(id));
            }
        }
        return result;
    }

    /**
     * 8.
     * @param humanMap
     * @return
     */
    public static List<Integer> ageMore18(Map<Integer, Human> humanMap) {
        List<Integer> result = new ArrayList<>();
        for(Integer id:humanMap.keySet()) {
            if(humanMap.get(id).getAge() >= 18) {
                result.add(id);
            }
        }
        return result;
    }

    /**
     * 9.
     * @param humanMap
     * @return
     */
    public static Map<Integer, Integer> getIdAgeMap(Map<Integer, Human> humanMap) {
        Map<Integer, Integer> result = new HashMap<>();
        Iterator<Integer> iteratorKey = humanMap.keySet().iterator();
        Iterator<Human> iteratorHuman = humanMap.values().iterator();
        int id;
        Human somebody;
        while(iteratorHuman.hasNext()) {
            somebody = iteratorHuman.next();
            id = iteratorKey.next();
            result.put(id, somebody.getAge());
        }
        return result;
    }

    /**
     * 10.
     * @param humanSet
     * @return
     */
    public static Map<Integer, List<Human>> getAgeHumanMap(Set<Human> humanSet) {
        Map<Integer, List<Human>> result = new HashMap<>();
        Iterator<Human> iteratorSet = humanSet.iterator();
        Human somebody;
        int age;
        while(iteratorSet.hasNext()) {
            somebody = iteratorSet.next();
            age = somebody.getAge();
            if(!result.containsKey(age)) {
                result.put(age, new ArrayList<Human>());
            }
            result.get(age).add(somebody);
        }
        return result;
    }

    /**
     * 11.
     * @param humanSet list of human
     * @return result complicated construction
     */
    public static Map<Integer, Map<Character, List<Human>>> getAgeCharacterHumanMap(Set<Human> humanSet) {
        Map<Integer, Map<Character, List<Human>>> result = new HashMap<>();
        Iterator<Human> iteratorSet = humanSet.iterator();
        Human somebody;
        int age;
        char firstLetter;
        Comparator<Human> comparator = new HumanNamesComparator();
        while(iteratorSet.hasNext()) {
            somebody = iteratorSet.next();
            age = somebody.getAge();
            firstLetter = somebody.getSecondName().charAt(0);
            if(!result.containsKey(age)) {
                result.put(age, new HashMap<Character, List<Human>>());
            }
            if(!result.get(age).containsKey(firstLetter)) {
                result.get(age).put(firstLetter, new ArrayList<Human>());
            }
            result.get(age).get(firstLetter).add(somebody);
            result.get(age).get(firstLetter).sort(comparator);
        }
        return result;
    }
}
