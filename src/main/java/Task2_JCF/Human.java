package Task2_JCF;

import java.util.Objects;

public class Human implements Comparable<Human> {
    private String secondName;
    private String firstName;
    private String middleName;
    private int age;

    public Human(String secondName, String firstName, String middleName, int age) {
        this.secondName = secondName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.age = age;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws HumanException {
        if(age < 0) {
            throw new HumanException("Incorrect age");
        }
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getAge() == human.getAge() &&
                Objects.equals(getSecondName(), human.getSecondName()) &&
                Objects.equals(getFirstName(), human.getFirstName()) &&
                Objects.equals(getMiddleName(), human.getMiddleName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSecondName(), getFirstName(), getMiddleName(), getAge());
    }

    @Override
    public int compareTo(Human human) {
        if(secondName.compareTo(human.secondName) == 0) {
            if(firstName.compareTo(human.firstName) == 0) {
                return middleName.compareTo(human.middleName);
            }
            return firstName.compareTo(human.firstName);
        }
        return secondName.compareTo(human.secondName);
    }

    @Override
    public String toString() {
        return "Human: " +
                secondName + " " + firstName + " " +
                middleName + " " +
                ", " + age ;
    }
}
