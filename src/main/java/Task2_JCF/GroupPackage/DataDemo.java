package Task2_JCF.GroupPackage;

import java.util.ArrayList;
import java.util.List;

public class DataDemo {
    public static List<Integer> getAll(Data data) {
        List<Integer> result = new ArrayList<>();
        DataIterator iterator = new DataIterator(data);
        while (iterator.hasNext()) {
            result.add(iterator.next());
        }
        return result;
    }
}
