package Task2_JCF.GroupPackage;

import java.util.Arrays;

public class Group {
    private int groupId;
    private int[] groupData;

    public Group(int id) {
        groupId = id;
        groupData = new int[]{};
    }
    public Group(int id, int ...data) {
        groupId = id;
        groupData = Arrays.copyOf(data, data.length);
    }

    public Group(Group copy) {
        groupId = copy.groupId;
        groupData = new int[copy.groupData.length];
        for(int i = 0; i < copy.groupData.length; i++) {
            groupData[i] = copy.groupData[i];
        }
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int[] getGroupData() {
        return groupData;
    }

    public void setGroupData(int[] groupData) {
        this.groupData = groupData;
    }

    public int getLength() {
        return groupData.length;
    }
}
