package Task2_JCF.GroupPackage;

import java.util.Iterator;

public class DataIterator implements Iterator<Integer> {
    private int index;
    private Data groups;

    public DataIterator(Data groupsSet) {
        index = -1;
        groups = groupsSet; //new Data(groupsSet);
    }

    @Override
    public boolean hasNext() {
        int size = 0;
        for(int i = 0; i < groups.getLength(); i++) {
            size += groups.getGroup(i).getLength();
        }
        return (index + 1) < size;
    }

    @Override
    public Integer next() {
        int currentIndex, groupIndex = 0;
        index++;
        currentIndex = index;
        while (currentIndex >= groups.getGroup(groupIndex).getLength()) {
            currentIndex -= groups.getGroup(groupIndex).getLength();
            groupIndex++;
        }
        return groups.getGroup(groupIndex).getGroupData()[currentIndex];
    }
}
