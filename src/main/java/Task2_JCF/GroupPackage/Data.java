package Task2_JCF.GroupPackage;

import java.util.Arrays;
import java.util.Iterator;

public class Data implements Iterable<Group>{
    private String dataName;
    private Group[] groups;

    public Data(String name) {
        dataName = name;
        groups = new Group[]{};
    }

    public Data(String name, Group ... groupsArr) {
        dataName = name;
        groups = Arrays.copyOf(groupsArr, groupsArr.length);
    }

    public Data(Data copy) {
        dataName = copy.dataName;
        groups = new Group[copy.getLength()];
        for(int i = 0; i < copy.getLength(); i++) {
            groups[i] = new Group(copy.groups[i]);
        }
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(Group[] groups) {
        this.groups = groups;
    }

    public int getLength() {
        return groups.length;
    }

    public Group getGroup(int index) {
        return groups[index];
    }

    @Override
    public Iterator<Group> iterator() {
        return null;
    }

    /*public DataIterator iterator() {
        DataIterator iterator = new DataIterator(this);
        return iterator;
    }*/

//    @Override
//    public Iterator<Group> iterator() {
//        return null;
//    }
}
