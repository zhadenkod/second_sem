package Task2_JCF;

public class Student extends Human {
    private String facultyName;

    public Student(String secondName, String firstName, String middleName, int age, String facultyName) {
        super(secondName, firstName, middleName, age);
        this.facultyName = facultyName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    @Override
    public String toString() {
        return "Student: " + getSecondName() + " " +
                getFirstName() + " " +
                getMiddleName() + " " +
                getAge() + " " + facultyName;
    }

}
