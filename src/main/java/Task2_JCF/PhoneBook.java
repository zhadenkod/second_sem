package Task2_JCF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class PhoneBook {
    private HashMap<Human, ArrayList<String>> phoneBook;

    public PhoneBook() {
        new HashMap<>();
    }

    public PhoneBook addPhoneNumber(String phoneNumber, Human somebody) { //можно ли как-то возвращать PhoneBook, чтобы через много точек добавлять
        if(!phoneBook.containsKey(somebody)) {
            ArrayList<String> numbers = new ArrayList<>();
            numbers.add(phoneNumber);
            phoneBook.put(somebody, numbers);
        } else {
            ArrayList<String> somebodysNumbers = phoneBook.get(somebody);
            somebodysNumbers.add(phoneNumber);
            phoneBook.put(somebody, somebodysNumbers);
        }
        return this;
    }

    public void deletePhoneNumber(String phoneNumber) {
        Iterator<Human> iteratorHumans = phoneBook.keySet().iterator();
        Human somebody;
        ArrayList<String> numbers;
        while(iteratorHumans.hasNext()) {
            somebody = iteratorHumans.next();
            numbers = phoneBook.get(somebody);
            if(numbers.remove(phoneNumber)) {
                phoneBook.put(somebody, numbers);
            }
        }
    }
    public ArrayList<String> getPhoneNumbersList(Human somebody) {
        return phoneBook.get(somebody);
    }

    public HashMap<Human, ArrayList<String>> getHumansWithSecondName(String beginning) {
        if (beginning == null ||beginning.equals("")) {
            throw new NullPointerException("Empty string!");
        }
        HashMap<Human, ArrayList<String>> result = new HashMap<>();
        for(Human human: phoneBook.keySet()) {
            boolean isEquals = true;
            for(int i = 0; i < beginning.length(); i++) {
                if(beginning.charAt(i) != human.getSecondName().charAt(i)) {
                    isEquals = false;
                }
            }
            if(isEquals) {
                result.put(human, phoneBook.get(human));
            }
        }
        return result;
    }
}
