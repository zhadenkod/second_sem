package Task4_Reflection;

public interface Executable {
    void execute();
}
