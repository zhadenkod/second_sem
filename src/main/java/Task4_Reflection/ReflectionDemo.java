package Task4_Reflection;

import Task2_JCF.Human;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {
    /**
     *
     * @param list list of Objects
     * @return amount of Objects of class Human in list
     */
    public static int amountOfHumans(List<Object> list) {
        int count = 0;
        for(Object obj: list) {
            if(obj instanceof Human) {
                count++;
            }
        }
        return count;
    }
    public static List<String> getPublicMethods(Object obj) {
        List<String> res = new ArrayList<>();
        for(Method method: obj.getClass().getMethods()) {
            res.add(method.getName());
        }
        return res;
    }
    public static List<String> getSuperClasses(Object obj) {
        List<String> res = new ArrayList<>();
        Class<? extends Object> cl = obj.getClass();
        while (cl != Object.class){
            cl = cl.getSuperclass();
            res.add(cl.getSimpleName());
        }
        return res;
    }
    public static int countExecutable(List<Object> list) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        int count = 0;
        for(Object obj: list) {
            if(obj instanceof Executable) {
                Method method = obj.getClass().getMethod("execute", (Class<?>) null);
                method.invoke(obj, (Object) null);
//                    ((Executable) obj).execute();
                count++;
            }
        }
        return count;
    }
    public static List<String> getGettersAndSetters(Object obj) {
        List<String> res = new ArrayList<>();
        for(Method method: obj.getClass().getMethods()) {
            int modifier = method.getModifiers();
            if(Modifier.isPublic(modifier) && !Modifier.isStatic(modifier) &&
                (
                    (method.getName().startsWith("get") && method.getReturnType() != null) ||
                    (method.getName().startsWith("set") && method.getReturnType() == null ))
            ) {
                res.add(method.getName());
            }
        }
        return res;
    }
}
