package Task5_Lyambda;

import java.util.Objects;

public class Student extends Human {
    private String universityName;
    private String facultyName;
    private String speciality;

    public Student(String secondName, String firstName, String middleName, int age, Sex sex,
                   String universityName, String facultyName, String speciality) {
        super(secondName, firstName, middleName, age, sex);
        this.universityName = universityName;
        this.facultyName = facultyName;
        this.speciality = speciality;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        if (!super.equals(o)) return false;
//        Student student = (Student) o;
//        return Objects.equals(universityName, student.universityName) &&
//                Objects.equals(facultyName, student.facultyName) &&
//                Objects.equals(speciality, student.speciality);
//    }

    @Override
    public String toString() {
        return "[Student:" + super.toString() +
                "facultyName = " + facultyName +
                ']';
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), universityName, facultyName, speciality);
    }
}
