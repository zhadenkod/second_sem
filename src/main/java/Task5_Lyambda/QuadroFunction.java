package Task5_Lyambda;

@FunctionalInterface
public interface QuadroFunction <T, R, S, U, F> {
    F apply(T t, R r, S s, U u);
}
