package Task5_Lyambda;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaRunner {

    public static <T, U> U resFunction (Function<T, U> func, T param) {
        return func.apply(param);
    }
    public static <T, R, S, U, F> F resQuadroFunction (QuadroFunction<T, R, S, U, F> func,
                                                      T param1, R param2, S param3, U param4) {
        return func.apply(param1, param2, param3, param4);
    }
    public static <T> boolean resPredicate (Predicate<T> pred, T argument) {
        return pred.test(argument);
    }
    public static <T, U> boolean resBiPredicate (BiPredicate<T, U> pred, T argument1, U argument2) {
        return pred.test(argument1, argument2);
    }

}
