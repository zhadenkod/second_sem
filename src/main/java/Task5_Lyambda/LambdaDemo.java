package Task5_Lyambda;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaDemo {
    public final static Function<String, Integer> strLength = String::length;
    public final static Function<String, Character> strFirstSymbol = (str) -> {
        if(str == null || str.equals("")){
            return null;
        }
        return str.charAt(0);
    };
    public final static Predicate<String> hasSpaces = (str) -> {
        for(int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == ' ') {
                return true;
            }
        }
        return false;
    };
    public final static Function<String, Integer> wordCount = (str) -> {
        if(str == null || str.equals("")){
            return 0;
        }
        String[] words = str.split(",");
        int count = 0;
        for(String string: words) {
            if(string != null && !string.equals("")) {
                count++;
            }
        }
        return count;
    };
    public final static Function<Human, Integer> humanAge = Human::getAge;
    public final static BiPredicate<Human, Human> hasEgualSecondName = (human1, human2) ->
            human1.getSecondName().equals(human2.getSecondName());
    public final static Function<Human, String> humanFullName = (human) ->
            human.getSecondName() + " " + human.getFirstName() + " " + human.getMiddleName();
    public final static Function<Human, Human> olderHuman = (human) -> {
        Human newHuman = new Human(human);
        newHuman.setAge(human.getAge() + 1);
        return newHuman;
    };
    public final static QuadroFunction<Human, Human, Human, Integer, Boolean> isHumansYoungerMaxAge =
            (human1, human2, human3, MaxAge) ->
                    human1.getAge() < MaxAge && human2.getAge() < MaxAge && human3.getAge() < MaxAge;
}
