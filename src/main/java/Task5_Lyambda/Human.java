package Task5_Lyambda;

import java.util.Objects;

public class Human {
    private String secondName;
    private String firstName;
    private String middleName;
    private int age;
    private Sex sex;

    public Human(String secondName, String firstName, String middleName, int age, Sex sex) {
        this.secondName = secondName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.age = age;
        this.sex = sex;
    }

    public Human(Human human) {
        this.secondName = human.secondName;
        this.firstName = human.firstName;
        this.middleName = human.middleName;
        this.age = human.age;
        this.sex = human.sex;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) { //EXCEPTION? if(age < 0) { throw new HumanException("Incorrect age"); }
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Human: " +
                secondName + " " + firstName + " " +
                middleName + " " +
                ", " + age + " " + sex ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(secondName, human.secondName) &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(middleName, human.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(secondName, firstName, middleName, age);
    }
}
