package Task5_Lyambda;

public class HumanException extends Exception {
    public HumanException() {}
    public HumanException(String message) {
        super(message);
    }
    public HumanException(Throwable cause) {
        super(cause);
    }
}
