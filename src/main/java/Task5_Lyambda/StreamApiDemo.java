package Task5_Lyambda;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamApiDemo extends LambdaDemo {
    public final static Function<List<Object>, List<Object>> deleteNull = list ->
            list.stream().filter(Objects::nonNull).collect(Collectors.toList());

    public final static Function<Set<Integer>, Integer> countPositive = set ->
            (int)set.stream().filter(item -> item > 0).count();

    public final static Function<List<Object>, List<Object>> getThreeLastObjects = list ->
            list.stream().skip(list.size() <= 3 ? 0 : list.size() - 3).collect(Collectors.toList());

    public final static Function<List<Integer>, Integer> getEven = list ->
            list.stream().filter(item -> item % 2 == 0).findFirst().orElse(null);

    public final static Function<int[], List<Integer>> getSquareList = array ->
            Arrays.stream(array).boxed().map(number -> number*number).distinct().collect(Collectors.toList());

    public final static Function<List<String>, List<String>> getNotEmptyStringList = list ->
            list.stream().filter(str -> !str.isEmpty()).sorted().collect(Collectors.toList());

    public final static Function<Set<String>, List<String>> getOrderedList = set ->
            set.stream().sorted((str1, str2) -> - str1.compareTo(str2)).collect(Collectors.toList());

    public final static Function<Set<Integer>, Integer> getSquaresSum = set ->
            set.stream().mapToInt(number -> number*number).sum();

    public final static Function<Collection<Human>, Integer> getMaxAge = humans ->
            humans.stream().map(humanAge).max(Integer::compareTo).orElse(-1);

    public final static Function<Collection<Human>, Collection<Human>> getSorted = humans ->
            humans.stream().sorted(Comparator.comparing(Human::getSex).thenComparing(Human::getAge)).collect(Collectors.toCollection(ArrayList::new));
}
